<?php session_start(); ?>
<?php
# Set both random numbers you want to add
$randomNum = rand(0,9);
$randomNum2 = rand(0,9);
# Get the total.
$randomNumTotal = $randomNum + $randomNum2;

$_SESSION['totalnum'] = $randomNumTotal;
?>

<script type="text/javascript" src="js/myjs.js"></script>

<script type="text/javascript" src="http://jqueryjs.googlecode.com/files/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript">
	$.validator.methods.equal = function(value, element, param) {
		return value == param;
	};
	$(document).ready(function(){
		$("#yourform").validate({
				rules: {
					lastName: "required",
					math: {
						equal: <?php echo $randomNumTotal; ?>	
					}
				},
				messages: {
					lastName: "*",
					math: ""
				}
			});
	});
	</script>
    
<style>

/*---CSS 3 @ Fontface---*/

form {
	font-size:11px;
	margin:0px 15px 15px 15px;
	background:#efefef;
	}

@font-face {
    font-family: 'lobster_twobold_italic';
    src: url('font/lobstertwo-bolditalic-webfont.eot');
    src: url('font/lobstertwo-bolditalic-webfont.eot?#iefix') format('embedded-opentype'),
         url('font/lobstertwo-bolditalic-webfont.woff') format('woff'),
         url('font/lobstertwo-bolditalic-webfont.ttf') format('truetype'),
         url('font/lobstertwo-bolditalic-webfont.svg#lobster_twobold_italic') format('svg');
    font-weight: normal;
    font-style: normal;

}

@font-face {
    font-family: 'lobster_twoitalic';
    src: url('font/lobstertwo-italic-webfont.eot');
    src: url('font/lobstertwo-italic-webfont.eot?#iefix') format('embedded-opentype'),
         url('font/lobstertwo-italic-webfont.woff') format('woff'),
         url('font/lobstertwo-italic-webfont.ttf') format('truetype'),
         url('font/lobstertwo-italic-webfont.svg#lobster_twoitalic') format('svg');
    font-weight: normal;
    font-style: normal;

}

@font-face {
    font-family: 'lobster_tworegular';
    src: url('font/lobstertwo-regular-webfont.eot');
    src: url('font/lobstertwo-regular-webfont.eot?#iefix') format('embedded-opentype'),
         url('font/lobstertwo-regular-webfont.woff') format('woff'),
         url('font/lobstertwo-regular-webfont.ttf') format('truetype'),
         url('font/lobstertwo-regular-webfont.svg#lobster_tworegular') format('svg');
    font-weight: normal;
    font-style: normal;

}
/*---CSS 3 @ Fontface End---*/


label {
	float: left;
	height:16px;
	clear: both;
	width: 280px;
	display: block;
	color:#5a5a5a;
	font-family:Arial, Helvetica, sans-serif;
}

input.field {
	border: 1px #ccc solid;
	margin-right: 5px;
	margin-top: 8px;
	width: 260px;
	height: 16px;
}

textarea.field {
	border: 1px #ccc solid;
	margin-right: 5px;
	margin-top: 8px;
	width: 260px;
	height: 48px;
}


input:focus.field {
	background: #ffffff;
}

/* BUTTONS */
/* Login and Register buttons */

.submit {
  margin: 0;
  padding: 0;
  border: 0;
  background: transparent url('images/btn_submit.png') no-repeat center bottom;
  text-indent: -9000em;
  text-transform: capitalize; 
  cursor: pointer; /* hand-shaped cursor */
  cursor: hand; /* for IE 5.x */
  display:inline-block; 
  zoom:1;
  *display:block;
  width:264px;
  height:40px;
}

.submit:hover {
  margin: 0px;
  padding:0;
  border: 0;
  background: transparent url('images/btn_submit_hover.png') no-repeat center bottom;
  text-indent: -9000em;
  text-transform: capitalize; 
  cursor: pointer; /* hand-shaped cursor */
  cursor: hand; /* for IE 5.x */
  display:inline-block;
  zoom:1;
  *display:block;
  width:264px;
  height:40px;
}

h1 {
	font-family: 'lobster_twoitalic', Helvetica, sans-serif;
	color:#5a5a5a;
	font-size: 1.6em;
	padding: 5px 0 10px;
	margin: 0;
	font-weight:normal;
	line-height:2em;
}

h2{
	font-family: 'lobster_tworegular', Helvetica, sans-serif;
	color:#5a5a5a;
	font-size: 1.2em;
	padding: 10px 0 5px;
	margin: 0;
	font-weight:normal;
}

p {
	margin: 5px 0;
	font-family:Arial, Helvetica, sans-serif;
	color:#5a5a5a;
	padding: 0;
}
</style>

<!-- the PHP tag inside the value of each textbox is to get back the value enter by the user after an validation or occur-->
<body onLoad="trigger()">

<!-- start jeff-->
<script language="JavaScript">
	var _error = "";
	
	function trigger(){
	  if( _error !=  ""){
	  //$("#open").trigger('click');
	  document.getElementById("tt").innerHTML="Wrong answer!";
	  
	  }

	}

	function gup( name ){
	name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");  
	var regexS = "[\\?&]"+name+"=([^&#]*)";  
	var regex = new RegExp( regexS );  
	var results = regex.exec( window.location.href ); 
	 if( results == null )    return "";  
	else
	_error = "";
	//alert(errors);
	
	return results[1];
	
	}
	
    var errors = gup( 'cap' );
	_error = errors;
	
	//alert(errors);
	
    </script>
    <!--end jeff-->

        <form name="form2" method="post" enctype="multipart/form-data" id="yourform">
        <h1 style="font-size:18px;">Write us a feedback!</h1>
        <label class="grey" style="font-size:11px;">Name: *</label>
        <input class="field" id="txtName" type="text" name="name" onChange="valName();" value="<?php echo $_SESSION['name']; ?>" size="23" />
        <label class="grey" style="font-size:11px;">Phone No:</label>
        <input class="field" type="text" name="mobile" id="txtMobile" size="23" onChange="valMobile();" value="<?php echo $_SESSION['mobile']; ?>"/>
        <label class="grey" style="font-size:11px;">Email: *</label>
        <input class="field" type="text" name="email_address" id="txtEmail" size="23" onChange="valEmail();" value="<?php echo $_SESSION['email']; ?>"/>
        <label class="grey" style="font-size:11px;">Message: *</label>
        <textarea class="field" type="text" name="comments" id="txaComments" size="23"/><?php echo $_SESSION['comments']; ?></textarea>
        
        <!-- start jeff captcha images generated-->
        <!--<br /><br />
        <label for='message'>Enter the code shown :</label>
                                <img src="captcha_code_file.php?rand=<?php echo rand(); ?>" id='captchaimg' ><br /><br />
                                <label for=''></label>
                                <input id="6_letters_code" name="6_letters_code" type="text"><br>
								<label for=''></label>
								<small>Can't read the image? click <a href='javascript: refreshCaptcha();'>here</a> to refresh</small>-->
                                
<!-- start jeff captcha images generated-->
        <label class="grey" style="font-size:11px;">Please solve this: *</label>
        <div style="padding-top:15px; padding-left:283px;">
<!-- end jeff-->
        
        <input type="text" name="captchaImage" size="5" value="<?= $randomNum ?> + <?= $randomNum2 ?>" disabled="disabled" style="text-align:center; font-weight:bold;"/> =
        <input type="text" name="math" id="math" size="6" maxlength="6" />
        <label id="tt" style="color:#fd4a4a"></label><br /><br />
        
        </div>
        <div style="width:545px; padding-top:25px;" align="right"><input  type="submit" value="Submit" class="submit" onClick="dataValidate();" /></div>
        </form> 
        
        <script language='JavaScript' type='text/javascript'>
function refreshCaptcha()
{
	var img = document.images['captchaimg'];
	img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
}
</script> 
</body>