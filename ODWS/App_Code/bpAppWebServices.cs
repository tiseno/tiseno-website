﻿using System;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using BizEntityLayer;
using BizDatabaseConnectionLayer;
using System.Data;
using System.Collections;
using System.Net;

[WebService(Namespace = "http://203.106.245.234:100/bpAppWebServices.asmx")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class bpAppWebServices : System.Web.Services.WebService
{
    public bpAppWebServices() 
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public BizEntityLogin[] login(string icno)
    {
        BizSqlProcess obj = new BizSqlProcess();
        DataSet dsLogin = obj.checkLogin(icno);
        int count = dsLogin.Tables[0].Rows.Count;
        BizEntityLogin bel = new BizEntityLogin();
        if (count != 0)
        {
            bel.resultCount = count.ToString();
            bel.LoginStatus = "success";
            bel.ICNO = dsLogin.Tables[0].Rows[0]["varUserID"].ToString();
        }
        else
        {
            bel.resultCount = count.ToString();
            bel.LoginStatus = "fail";
            bel.ICNO = "none";
        }
        ArrayList list = new ArrayList();
        list.Add(bel);

        BizEntityLogin[] belArr = new BizEntityLogin[1];
        belArr[0] = (BizEntityLogin)list[0];
        return belArr;
    }

    [WebMethod]
    public BizEntityPoints[] retrieveRewardRebatePoints(string userid)
    {
        BizSqlProcess obj = new BizSqlProcess();
        DataSet dsPointBalance = obj.getPointBalance(userid);
        int count = dsPointBalance.Tables[0].Rows.Count;
        
        ArrayList list = new ArrayList();
        if (count != 0)
        {
            BizEntityPoints bep = new BizEntityPoints();
            bep.resultCount = count.ToString();
            bep.PointEarn = dsPointBalance.Tables[0].Rows[0]["PointEarn"].ToString();
            bep.PointRebate = dsPointBalance.Tables[0].Rows[0]["PointRebate"].ToString();
            list.Add(bep);
        }
        else
        {
            BizEntityPoints bep = new BizEntityPoints();
            bep.resultCount = count.ToString();
            bep.PointEarn = "none";
            bep.PointRebate = "none";
            list.Add(bep);
        }
        
        BizEntityPoints[] bepArr = new BizEntityPoints[1];
        bepArr[0] = (BizEntityPoints)list[0];
        
        return bepArr;
    }
    [WebMethod]
    public BizEntityOutlet[] retrieveOutletInfo(string state)
    {
        BizSqlProcess obj = new BizSqlProcess();
        DataSet dsOutletInfo = obj.getOutletInfo(state);
        int count = dsOutletInfo.Tables[0].Rows.Count;
        ArrayList list = new ArrayList();
        BizEntityOutlet[] betArr = null;
        if (count != 0)
        {
            for (int i = 0; i < count; i++)
            {
                BizEntityOutlet bet = new BizEntityOutlet();
                bet.resultCount = (i + 1).ToString();
                bet.BranchName = dsOutletInfo.Tables[0].Rows[i]["BranchName"].ToString();
                bet.Address_1 = dsOutletInfo.Tables[0].Rows[i]["Address_1"].ToString();
                bet.Address_2 = dsOutletInfo.Tables[0].Rows[i]["Address_2"].ToString();
                bet.Address_3 = dsOutletInfo.Tables[0].Rows[i]["Address_3"].ToString();
                bet.Tel = dsOutletInfo.Tables[0].Rows[i]["Tel"].ToString();
                bet.Fax = dsOutletInfo.Tables[0].Rows[i]["Fax"].ToString();
                string strMap = "";
                string latitude = "";
                string longitude = "";
                if (dsOutletInfo.Tables[0].Rows[i]["MapURL"].ToString() != string.Empty)
                {
                    strMap = dsOutletInfo.Tables[0].Rows[i]["MapURL"].ToString();
                    int intBr = strMap.IndexOf("<br");
                    strMap = strMap.Remove(intBr);
                    int intUrl = strMap.IndexOf("ll=");
                    strMap = strMap.Remove(0, intUrl);
                    int intEndUrl = strMap.IndexOf("spn=");
                    strMap = strMap.Remove(intEndUrl);
                    int intEql = strMap.IndexOf("=");
                    int intAmp = strMap.IndexOf("&");
                    strMap = strMap.Remove(intAmp);
                    strMap = strMap.Remove(0, intEql + 1);
                    int intComma = strMap.IndexOf(",");
                    latitude = strMap.Substring(0, intComma);
                    longitude = strMap.Substring(intComma + 1);
                }

                bet.Latitude = latitude;
                bet.Longitude = longitude;
                bet.TypeDC = dsOutletInfo.Tables[0].Rows[i]["TypeDC"].ToString();
                bet.TypeDISPENSARY = dsOutletInfo.Tables[0].Rows[i]["TypeLAB"].ToString();
                bet.TypeHEARING = dsOutletInfo.Tables[0].Rows[i]["TypeHEARING"].ToString();
                bet.TypeLAB = dsOutletInfo.Tables[0].Rows[i]["TypeDISPENSARY"].ToString();
                bet.BusinessHourImg = dsOutletInfo.Tables[0].Rows[i]["BusinessHourImg"].ToString();
                list.Add(bet); 
            }
            betArr = new BizEntityOutlet[count];
            for (int i = 0; i < list.Count; i++)
            {
                betArr[i] = (BizEntityOutlet)list[i];
            }
        }
        else
        {
            BizEntityOutlet bet = new BizEntityOutlet();
            bet.resultCount = count.ToString();
            bet.BranchName = "none";
            bet.Address_1 = "none";
            bet.Address_2 = "none";
            bet.Address_3 = "none";
            bet.Tel = "none";
            bet.Fax = "none";
            bet.Latitude = "none";
            bet.Longitude = "none";
            bet.TypeDC = "none";
            bet.TypeDISPENSARY = "none";
            bet.TypeHEARING = "none";
            bet.TypeLAB = "none";
            bet.BusinessHourImg = "none";
            list.Add(bet);

            betArr = new BizEntityOutlet[1];
            betArr[0] = (BizEntityOutlet)list[0];
        }
        return betArr;
    }
    [WebMethod]
    public BizEntityAppointment[] retrieveAppointmentList(string tokenId)
    {
        BizSqlProcess obj = new BizSqlProcess();
        DataSet dsAppList = obj.getAppointmentList(tokenId);
        int count = dsAppList.Tables[0].Rows.Count;
        ArrayList list = new ArrayList();
        BizEntityAppointment[] beaArr = null;
        if (count != 0)
        {
            for (int i = 0; i < count; i++)
            {
                BizEntityAppointment bea = new BizEntityAppointment();
                bea.resultCount = (i + 1).ToString();
                bea.appointmentServiceRequest = dsAppList.Tables[0].Rows[i]["appointmentServiceRequest"].ToString();
                bea.appointmentCreateDate = dsAppList.Tables[0].Rows[i]["appointmentCreateDate"].ToString();
                bea.appointmentBookDateTime = dsAppList.Tables[0].Rows[i]["appointmentBookDateTime"].ToString();
                bea.customerName = dsAppList.Tables[0].Rows[i]["customerName"].ToString();
                bea.customerContact = dsAppList.Tables[0].Rows[i]["customerContact"].ToString();
                bea.customerEmail = dsAppList.Tables[0].Rows[i]["customerEmail"].ToString();
                bea.appointmentBranch = dsAppList.Tables[0].Rows[i]["appointmentBranch"].ToString();
                bea.appointmentNoOfPerson = dsAppList.Tables[0].Rows[i]["appointmentNoOfPerson"].ToString();
                bea.appointmentStatus = dsAppList.Tables[0].Rows[i]["appointmentStatus"].ToString();
                bea.customerRemark = dsAppList.Tables[0].Rows[i]["customerRemark"].ToString();
                bea.adminRemark = dsAppList.Tables[0].Rows[i]["adminRemark"].ToString();
                bea.userID = dsAppList.Tables[0].Rows[i]["userID"].ToString();
                list.Add(bea);
            }
            beaArr = new BizEntityAppointment[count];
            for (int i = 0; i < list.Count; i++)
            {
                beaArr[i] = (BizEntityAppointment)list[i];
            }
        }
        else
        {
            BizEntityAppointment bea = new BizEntityAppointment();
            bea.resultCount = count.ToString();
            bea.appointmentServiceRequest = "none";
            bea.appointmentCreateDate = "none";
            bea.appointmentBookDateTime = "none";
            bea.customerName = "none";
            bea.customerContact = "none";
            bea.customerEmail = "none";
            bea.appointmentBranch = "none";
            bea.appointmentNoOfPerson = "none";
            bea.appointmentStatus = "none";
            bea.customerRemark = "none";
            bea.adminRemark = "none";
            bea.userID = "none";
            list.Add(bea);

            beaArr = new BizEntityAppointment[1];
            beaArr[0] = (BizEntityAppointment)list[0];
        }
        return beaArr;
    }
    [WebMethod]
    public BizEntityAppointment[] saveAppointment(string userID, string serviceReq, string appDate, string customerName, string customerContact, string customerEmail, string appBranch, string noOfPerson, string custRemark, string tokenID)
    {
        BizSqlProcess obj = new BizSqlProcess();
        int result = obj.insertAppointment(userID, serviceReq, appDate, customerName, customerContact, customerEmail, appBranch, noOfPerson, custRemark, tokenID);
        BizEntityAppointment bea = new BizEntityAppointment();
        if (result != 0)
        {
            bea.insertResult = "success";
        }
        else
        {
            bea.insertResult = "fail";
        }
        ArrayList list = new ArrayList();
        list.Add(bea);
        BizEntityAppointment[] beaArr = new BizEntityAppointment[1];
        beaArr[0] = (BizEntityAppointment)list[0];
        return beaArr;
    }
    [WebMethod]
    public BizEntityCheckUp[] retrieveCheckUpList()
    {
        BizSqlProcess obj = new BizSqlProcess();
        DataSet dsCheckUp = obj.getCheckUpList();
        int count = dsCheckUp.Tables[0].Rows.Count;
        ArrayList list = new ArrayList();
        BizEntityCheckUp[] becArr = null;
        if (count != 0)
        {
            for (int i = 0; i < count; i++)
            {
                BizEntityCheckUp bec = new BizEntityCheckUp();
                bec.resultCount = (i + 1).ToString();
                bec.checkupImgPath = dsCheckUp.Tables[0].Rows[i]["checkupImgPath"].ToString();
                bec.checkupTitle = dsCheckUp.Tables[0].Rows[i]["checkupTitle"].ToString();
                bec.checkupDescp = dsCheckUp.Tables[0].Rows[i]["checkupDescp"].ToString();
                list.Add(bec);
            }

            becArr = new BizEntityCheckUp[count];
            for (int i = 0; i < list.Count; i++)
            {
                becArr[i] = (BizEntityCheckUp)list[i];
            }
        }
        else
        {
            BizEntityCheckUp bec = new BizEntityCheckUp();
            bec.resultCount = count.ToString();
            bec.checkupImgPath = "none";
            bec.checkupTitle = "none";
            bec.checkupDescp = "none";
            list.Add(bec);

            becArr = new BizEntityCheckUp[1];
            becArr[0] = (BizEntityCheckUp)list[0];
        }
        return becArr;
    }
    [WebMethod]
    public BizEntityPromotion[] retrievePromotionList()
    {
        BizSqlProcess obj = new BizSqlProcess();
        DataSet dsPromotion = obj.getPromotionsList();
        int count = dsPromotion.Tables[0].Rows.Count;
        ArrayList list = new ArrayList();
        BizEntityPromotion[] bepArr = null;
        if (count != 0)
        {
            for (int i = 0; i < count; i++)
            {
                BizEntityPromotion bep = new BizEntityPromotion();
                bep.resultCount = (i + 1).ToString();
                bep.promotionImgPath = dsPromotion.Tables[0].Rows[i]["promotionImgPath"].ToString();
                bep.promotionTitle = dsPromotion.Tables[0].Rows[i]["promotionTitle"].ToString();
                bep.promotionDescp = dsPromotion.Tables[0].Rows[i]["promotionDescp"].ToString();
                list.Add(bep);
            }
            bepArr = new BizEntityPromotion[count];
            for (int i = 0; i < list.Count; i++)
            {
                bepArr[i] = (BizEntityPromotion)list[i];
            }
        }
        else
        {
            BizEntityPromotion bep = new BizEntityPromotion();
            bep.resultCount = count.ToString();
            bep.promotionImgPath = "none";
            bep.promotionTitle = "none";
            bep.promotionDescp = "none";
            list.Add(bep);

            bepArr = new BizEntityPromotion[1];
            bepArr[0] = (BizEntityPromotion)list[0];
        }
        return bepArr;
    }
    [WebMethod]
    public BizEntityHealthTips[] retrieveHealthTipsList()
    {
        BizSqlProcess obj = new BizSqlProcess();
        DataSet dsHealthTips = obj.getHealthTipsList();
        int count = dsHealthTips.Tables[0].Rows.Count;
        ArrayList list = new ArrayList();
        BizEntityHealthTips[] behArr = null;
        if (count != 0)
        {
            for (int i = 0; i < count; i++)
            {
                BizEntityHealthTips beh = new BizEntityHealthTips();
                beh.resultCount = (i + 1).ToString();
                beh.healthtipsImgPath = dsHealthTips.Tables[0].Rows[i]["healthtipsImgPath"].ToString();
                beh.healthtipsTitle = dsHealthTips.Tables[0].Rows[i]["healthtipsTitile"].ToString();
                beh.healthtipsDescp = dsHealthTips.Tables[0].Rows[i]["healthtipsDescp"].ToString();
                list.Add(beh);
            }
            behArr = new BizEntityHealthTips[count];
            for (int i = 0; i < list.Count; i++)
            {
                behArr[i] = (BizEntityHealthTips)list[i];
            }
        }
        else
        {
            BizEntityHealthTips beh = new BizEntityHealthTips();
            beh.resultCount = count.ToString();
            beh.healthtipsImgPath = "none";
            beh.healthtipsTitle = "none";
            beh.healthtipsDescp = "none";
            list.Add(beh);

            behArr = new BizEntityHealthTips[1];
            behArr[0] = (BizEntityHealthTips)list[0];
        }
        return behArr;
    }
    [WebMethod]
    public BizEntityCoupon[] retrieveCouponList()
    {
        BizSqlProcess obj = new BizSqlProcess();
        DataSet dsCoupon = obj.getCouponsList();
        int count = dsCoupon.Tables[0].Rows.Count;
        ArrayList list = new ArrayList();
        BizEntityCoupon[] becArr = null;
        if (count != 0)
        {
            for (int i = 0; i < count; i++)
            {
                BizEntityCoupon bec = new BizEntityCoupon();
                bec.resultCount = (i + 1).ToString();
                bec.couponImgPath = dsCoupon.Tables[0].Rows[i]["couponImgPath"].ToString();
                bec.couponTitle = dsCoupon.Tables[0].Rows[i]["couponTitle"].ToString();
                bec.couponDescp = dsCoupon.Tables[0].Rows[i]["couponDescp"].ToString();
                list.Add(bec);
            }
            becArr = new BizEntityCoupon[count];
            for (int i = 0; i < list.Count; i++)
            {
                becArr[i] = (BizEntityCoupon)list[i];
            }
        }
        else
        {
            BizEntityCoupon bec = new BizEntityCoupon();
            bec.resultCount = count.ToString();
            bec.couponImgPath = "none";
            bec.couponTitle = "none";
            bec.couponDescp = "none";
            list.Add(bec);

            becArr = new BizEntityCoupon[1];
            becArr[0] = (BizEntityCoupon)list[0];
        }
        return becArr;
    }
    [WebMethod]
    public BizEntityEvents[] retrieveEventsList()
    {
        BizSqlProcess obj = new BizSqlProcess();
        DataSet dsEvents = obj.getEventsList();
        int count = dsEvents.Tables[0].Rows.Count;
        ArrayList list = new ArrayList();
        BizEntityEvents[] beeArr = null;
        if (count != 0)
        {
            for (int i = 0; i < count; i++)
            {
                BizEntityEvents bee = new BizEntityEvents();
                bee.resultCount = (i + 1).ToString();
                bee.eventsImgPath = dsEvents.Tables[0].Rows[i]["eventsImgPath"].ToString();
                bee.eventsTitle = dsEvents.Tables[0].Rows[i]["eventsTitle"].ToString();
                bee.eventsDescp = dsEvents.Tables[0].Rows[i]["eventsDescp"].ToString();
                bee.eventsDateTime = dsEvents.Tables[0].Rows[i]["eventsDateTime"].ToString();
                list.Add(bee);
            }
            beeArr = new BizEntityEvents[count];
            for (int i = 0; i < list.Count; i++)
            {
                beeArr[i] = (BizEntityEvents)list[i];
            }
        }
        else
        {
            BizEntityEvents bee = new BizEntityEvents();
            bee.resultCount = count.ToString();
            bee.eventsImgPath = "none";
            bee.eventsTitle = "none";
            bee.eventsDescp = "none";
            bee.eventsDateTime = "none";
            list.Add(bee);

            beeArr = new BizEntityEvents[1];
            beeArr[0] = (BizEntityEvents)list[0];
        }
        return beeArr;
    }
    [WebMethod]
    public BizEntityFestival[] retrieveFestival()
    {
        BizSqlProcess obj = new BizSqlProcess();
        DataSet dsFestival = obj.getFestivalList();
        int count = dsFestival.Tables[0].Rows.Count;
        ArrayList list = new ArrayList();
        BizEntityFestival[] befArr = null;
        if (count != 0)
        {
            for (int i = 0; i < count; i++)
            {
                BizEntityFestival bef = new BizEntityFestival();
                bef.resultCount = (i + 1).ToString();
                bef.festivalImgPath = dsFestival.Tables[0].Rows[i]["festivalImgPath"].ToString();
                bef.festivalTitle = dsFestival.Tables[0].Rows[i]["festivalTitle"].ToString();
                bef.festivalDescp = dsFestival.Tables[0].Rows[i]["festivalDescp"].ToString();
                list.Add(bef);
            }
            befArr = new BizEntityFestival[count];
            for (int i = 0; i < list.Count; i++)
            {
                befArr[i] = (BizEntityFestival)list[i];
            }
        }
        else
        {
            BizEntityFestival bef = new BizEntityFestival();
            bef.resultCount = count.ToString();
            bef.festivalImgPath = "none";
            bef.festivalTitle = "none";
            bef.festivalDescp = "none";
            list.Add(bef);

            befArr = new BizEntityFestival[1];
            befArr[0] = (BizEntityFestival)list[0];
        }
        return befArr;
    }
    [WebMethod]
    public BizEntityBirthdayTreatImage[] retrieveBirthdayTreatsImage()
    {
        BizSqlProcess obj = new BizSqlProcess();
        DataSet dsBirth = obj.getBirthdayTreatsImage();
        int count = dsBirth.Tables[0].Rows.Count;
        ArrayList list = new ArrayList();
        BizEntityBirthdayTreatImage[] bebtArr = null;
        if (count != 0)
        {
            for (int i = 0; i < count; i++)
            {
                BizEntityBirthdayTreatImage bebt = new BizEntityBirthdayTreatImage();
                bebt.resultCount = (i + 1).ToString();
                bebt.birthday_imgPath = dsBirth.Tables[0].Rows[i]["birthday_imgPath"].ToString();
                list.Add(bebt);
            }
            bebtArr = new BizEntityBirthdayTreatImage[count];
            for (int i = 0; i < list.Count; i++)
            {
                bebtArr[i] = (BizEntityBirthdayTreatImage)list[i];
            }
        }
        else
        {
            BizEntityBirthdayTreatImage bebt = new BizEntityBirthdayTreatImage();
            bebt.resultCount = count.ToString();
            bebt.birthday_imgPath = "none";
            list.Add(bebt);

            bebtArr = new BizEntityBirthdayTreatImage[1];
            bebtArr[0] = (BizEntityBirthdayTreatImage)list[0];
        }
        return bebtArr;
    }
}
