﻿using System;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.Collections;
using my.com.orientaldaily.www;

[WebService(Namespace = "http://tiseno.com/ODWS/orientaldailyWS.asmx")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class orientaldailyWS : System.Web.Services.WebService
{
    public orientaldailyWS () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public responseGetCategoryByID[] Get_Category()
    {
        OrientalServices1 os = new OrientalServices1();

        return os.Get_Category();
    }
    [WebMethod]
    public responseGetCommentByNewsID[] Get_CommentByNewsID(string itemid)
    {
        OrientalServices1 os = new OrientalServices1();

        return os.Get_CommentByNewsID(int.Parse(itemid));
    }
    [WebMethod]
    public responseGetTopNewsByCategory[] GetTopNewsByCategory(string catid,string fromIndex)
    {
        OrientalServices1 os = new OrientalServices1();

        return os.GetTopNewsByCategory(int.Parse(catid),fromIndex);
    }
    [WebMethod]
    public responseGetTopNewsByCategoryOverall[] GetTopNewsByCategoryOverall()
    {
        OrientalServices1 os = new OrientalServices1();

        return os.GetTopNewsByCategoryOverall();
    }
    [WebMethod]
    public responseGetCountComment[] GetCountComment(string itemid)
    {
        OrientalServices1 os = new OrientalServices1();

        return os.GetCountComment(int.Parse(itemid));
    }
    [WebMethod]
    public responseInsertComment[] InsertComment(string itemid,string name,string comment,string approvalStatus)
    {
        OrientalServices1 os = new OrientalServices1();

        return os.Insert_Comment(int.Parse(itemid), name, comment, approvalStatus);
    }
    [WebMethod]
    public responseInsertEnquiry[] InsertEnquiry(string name,string email,string suggestion,string remark)
    {
        OrientalServices1 os = new OrientalServices1();

        return os.Insert_enquiry(name, email, suggestion, remark);
    }
    [WebMethod]
    public responseInsertOrder[] InsertOrder(string type,string chineseName,string name,string address,string email,string contNo,string remark)
    {
        OrientalServices1 os = new OrientalServices1();

        return os.Insert_order(type, chineseName, name, address, email, contNo, remark);
    }
    [WebMethod]
    public responseInsertSurveyBB[] InsertSurvey(string name,string email,string age,string gender,string job,string income,string interest)
    {
        OrientalServices1 os = new OrientalServices1();

        return os.Insert_survey(name, email, age, gender, job, income, interest);
    }
    [WebMethod]
    public responseInsertSurveyIOS[] InsertSurveyIOS(string name, string email, string age, string gender, string job, string income, string interest, string iosTokenID)
    {
        OrientalServices1 os = new OrientalServices1();

        return os.Insert_survey_tokenIOS(name, email, age, gender, job, income, interest, iosTokenID);
    }
}
