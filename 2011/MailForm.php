<?php

$my_email = "terrence@tiseno.com, zack@tiseno.com";

$continue = "http://www.tiseno.com";

if ($_SERVER['REQUEST_METHOD'] != "POST"){exit;}

// Check for disallowed characters in the Name and Email fields.

$disallowed_name = array(':',';',"'",'"','=','(',')','{','}','@');

foreach($disallowed_name as $value)
{

if(stristr($_POST[Name],$value)){header("location: $_SERVER[HTTP_REFERER]");exit;}

}

$disallowed_email = array(':',';',"'",'"','=','(',')','{','}');

foreach($disallowed_email as $value)
{

if(stristr($_POST[Email],$value)){header("location: $_SERVER[HTTP_REFERER]");exit;}

}

$message = "";

// This line prevents a blank form being sent

while(list($key,$value) = each($_POST))
{if(!(empty($value)))
{
	$set=1;
}
	$message = $message . "$key: $value\n\n";
} 

if($set!==1)
{
	header("location: $_SERVER[HTTP_REFERER]");
	exit;
}

//$message = $message . "-- \nThank you ... from http://www.tiseno.com";
$message = stripslashes($message);

$subject = "Enquiry - Contact Us @ www.tiseno.com";

$headers = "From: " . $_POST['Email'] . "\n" . "Return-Path: " . $_POST['Email'] . "\n" . "Reply-To: " . $_POST['Email'] . "\n";

mail($my_email,$subject,$message,$headers);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/maintempt.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- InstanceBeginEditable name="doctitle" -->
<title>Tiseno Integrated Solutions</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-image: url(images/subpage-bg.jpg);
	background-repeat: no-repeat;
	background-color: #3A3A39;
	background-position: center top;
}
-->
</style>
<script type="text/JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function getRBtnID(title) { 
  var obj = document.getElementById(title); 
// next line is collects information about options 
  var sel = document.getElementsByName(obj.name); 
  var fnd = -1; 
  for (var i=0; i<sel.length; i++) { 
    if (sel[i].checked == true) { fnd = i; break; } 
  } 
  return fnd; 
// return -1 if not checked and +0...+n (where n= number of options) 
// 0 = NO and 1 = YES as options or whatever order you have defined 
} 

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  /*if (getRBtnID(title)<0){
    errors += 'Bankrupcy status must be checked';
  }*/
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
  } 
  if (errors) alert('The following error(s) occurred:\n'+errors);
  document.MM_returnValue = (errors == '');
}
//-->
</script>
<script type="text/javascript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/scriptaculous.js?load=effects,builder"></script>
<script type="text/javascript" src="js/lightbox.js"></script>
<link rel="stylesheet" href="css/lightbox.css" type="text/css" media="screen" />
<link href="extras/styles.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body,td,th {
	font-family: Tahoma;
	font-size: 11px;
	color: #333333;
}
-->
</style></head>

<body onload="MM_preloadImages('images/subpage-ecomm-roll.gif','images/subpage-do-u-kow-over.gif')">
<!-- DO NOT MOVE! The following AllWebMenus code must always be placed right AFTER the BODY tag-->
<!-- ******** BEGIN ALLWEBMENUS CODE FOR menu ******** -->
<span id='xawmMenuPathImg-menu' style='position:absolute;top:-50px'><img name='awmMenuPathImg-menu' id='awmMenuPathImg-menu' src='extras/awmmenupath.gif' alt='' /></span>
<script type='text/javascript'>var MenuLinkedBy='AllWebMenus [2]', awmBN='DW'; awmAltUrl='';</script>
<script src='extras/menu.js' language='JavaScript1.2' type='text/javascript'></script>
<script type='text/javascript'>awmBuildMenu();</script>
<!-- ******** END ALLWEBMENUS CODE FOR menu ******** -->
<table width="750" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td colspan="2" valign="top"><table width="750" height="9" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="750" height="9" background="images/top-bar.gif"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="146" valign="top"><table width="146" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td valign="top"><img src="images/subpage-logo.gif" width="146" height="133" /></td>
      </tr>
      <tr>
        <td height="144" valign="top"><style type="text/css">
        .awmAnchor {position:relative;z-index:0}
        </style>
              <span id='awmAnchor-menu' class='awmAnchor'></span></td>
      </tr>
      <tr>
        <td valign="top" background="images/grey-bg.jpg" bgcolor="#dadada"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="images/subpage-ecomm.jpg" width="146" height="112" /></td>
          </tr>
          <tr>
            <td><a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image5','','images/subpage-ecomm-roll.gif',1)"><img src="images/subpage-ecomm.gif" name="Image5" width="146" height="22" border="0" id="Image5" /></a></td>
          </tr>
          <tr>
            <td height="12"></td>
          </tr>
          <tr>
            <td><a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image6','','images/subpage-do-u-kow-over.gif',1)"><img src="images/subpage-do-u-kow.gif" name="Image6" width="146" height="67" border="0" id="Image6" /></a></td>
          </tr>
          
        </table></td>
      </tr>
      
    </table></td>
    <td width="604" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="604" height="133" valign="top"><!-- InstanceBeginEditable name="banner" --><img src="images/banner-enquired.jpg" width="604" height="133" /><!-- InstanceEndEditable --></td>
      </tr>
      
      <tr>
        <td height="18"></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="3%" height="18"></td>
            <td width="94%"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="4%"><img src="images/bullet-03.gif" width="16" height="15" /></td>
                <td width="96%" class="title"><!-- InstanceBeginEditable name="title" --><!-- InstanceEndEditable --></td>
              </tr>
            </table></td>
            <td width="3%"></td>
          </tr>
          
          <tr>
            <td height="16" colspan="3"></td>
            </tr>
          <tr>
            <td></td>
            <td><!-- InstanceBeginEditable name="content" -->
          <p class="enquiry">Thank you <?php print stripslashes($_POST['Name']); ?>!</p>
		  <p class="enquiry">Your enquiry form has been sent to <?php echo "$my_email"; ?></p>
          <p class="enquiry">We will get back to you as soon as possible</p>
          <p><a href="<?php print "$continue"; ?>" class="url">Click here to continue</a></p><!-- InstanceEndEditable --></td>
            <td></td>
          </tr>
          
        </table></td>
      </tr>
      
      <tr>
        <td height="25"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="38" colspan="2" bgcolor="#000000"><div align="center">
      <table width="750" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="150"></td>
          <td width="600" class="main-footer2a">1024x768 screen resolution   |   Flash 6 above<br />
            Copyright Tiseno Integrated Solutions 2009. All rights reserved.</td>
        </tr>
      </table>
    </div></td>
  </tr>
</table>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-1851989-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>
</body>
<!-- InstanceEnd --></html>
