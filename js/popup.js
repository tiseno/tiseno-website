// JavaScript Document

$(document).ready(function() {

    $('#lightbox_button').click(function() {
         $('#lightbox').fadeIn('1000');
		 $('#lightbox').fadeTo("slow",0.8);
         $('#lightbox_panel').fadeIn('slow');
    });

    $('#close_lightbox').click(function() {
        $('#lightbox').fadeOut('slow');
        $('#lightbox_panel').fadeOut('slow');
    });
	
	var id = '#lightbox_panel';

	//Get the window height and width
	var winH = $(window).height();
	var winW = $(window).width();
              
	//Set the popup window to center
	//$(id).css('top',  winH/2-$(id).height()/2);
	$(id).css('left', winW/2-$(id).width()/2);
	
	//if mask is clicked
	$('#lightbox').click(function () {
		$(this).hide();
		$('#lightbox_panel').hide();
	});

});