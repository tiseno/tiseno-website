<?php session_start(); ?>

<?php
# Set both random numbers you want to add
$randomNum = rand(0,9);
$randomNum2 = rand(0,9);
# Get the total.
$randomNumTotal = $randomNum + $randomNum2;

$_SESSION['totalnum'] = $randomNumTotal;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="verify-v1" content="BgvF5d4vTKD/iu1f6m7qp2VGkQbp1mi+Cn0VIDiJC1Y=" />
	<meta name="description" content="Tiseno is the mobile developer in malaysia which has expertise in all popular mobile application platform; Android, iPhone, BlackBerry and Windows Phone."/>
	<meta name="keywords" Content="Tiseno, mobile applications, mobile solutions, mobile media marketing, mobile games development, apps developer, mobile developer in malaysia, iphone app developer, android developer, app store developer">
	<meta name="created by" content="Malaysia Mobile Solutions, Malaysia Mobile, Malaysia Mobile Content Management, http://www.tiseno.com">
	<meta name="copyright" content="Tiseno Integrated Solutions., 2007-2011">
	<meta name="GOOGLEBOT" CONTENT="index, follow">
	<meta name="revisit-after" content="2 days">
	<link rel="shortcut icon" href="/favicon.gif" />
    <link href="css/stylesheet.css" rel="stylesheet" type="text/css" media="screen">
    
    <script type="text/javascript" src="http://jqueryjs.googlecode.com/files/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/jquery.validate.min.js"></script>
<script type="text/javascript">
	$.validator.methods.equal = function(value, element, param) {
		return value == param;
	};
	$(document).ready(function(){
		$("#yourform").validate({
				rules: {
					lastName: "required",
					math: {
						equal: <?php echo $randomNumTotal; ?>	
					}
				},
				messages: {
					lastName: "*",
					math: ""
				}
			});
	});
	</script>
    
    <script src="js/modernizr-2.6.1.min.js"></script>
    
    <!---Feedback--->
    <link rel="stylesheet" href="css/slide.css" type="text/css" media="screen" />
	
  	<!-- PNG FIX for IE6 -->
  	<!-- http://24ways.org/2007/supersleight-transparent-png-in-ie6 -->
	<!--[if lte IE 6]>
		<script type="text/javascript" src="js/pngfix/supersleight-min.js"></script>
	<![endif]-->
	 
    <!-- jQuery - the core -->
	<script src="js/jquery-1.3.2.min.js" type="text/javascript"></script>
	<!-- Sliding effect -->
	<script src="js/slide.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/myjs.js"></script>
    <!---Feedback End--->
    <!---Quotation--->
    <script src="js/popup.js" type="text/javascript"></script>
    <!---Quotation End--->
    
    <!--Image Preload--->
    <script language="JavaScript">
    if (document.images)
    {
      preload_image_object = new Image();
      // set image url
      image_url = new Array();
      image_url[0] = "images/facebook_hover.png";
	  image_url[1] = "images/request_quote_hover.png";
	  image_url[2] = "images/btn_submit_hover.png";
	  image_url[3] = "images/close_hover.png";
	  image_url[4] = "images/btn_submit_hover.png";

       var i = 0;
       for(i=0; i<=4; i++) 
         preload_image_object.src = image_url[i];
    }

    </script>     
    <!--Image Preload End--->
    
    <title>Tiseno Integrated Solutions, Mobile Application, Mobile Solution, Malaysia Mobile Solutions, Malaysia Mobile, Malaysia Mobile Application, http://www.tiseno.com</title>
</head>
<body onLoad="trigger()" bgcolor="#efefef">

<!-- start jeff-->
   <script language="JavaScript">
	var _error = "";
	var _error_name = "";
	
	function trigger(){
	  if( _error !=  ""){
	  
	  document.getElementById("errtxt").innerHTML="Wrong answer!";
	  }
	  
	  if( _error_name !=  ""){
	  
	  document.getElementById("lblname").innerHTML="Fill in Name!";
	  }
	  
	  if( _error_contact !=  ""){
	  
	  document.getElementById("lblcontact").innerHTML="Fill in Contact No!";
	  }
	  
	  if( _error_email !=  ""){
	  
	  document.getElementById("lblemail").innerHTML="Fill in Email Address!";
	  }
	}
	  
    function gup( name ){
	name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");  
	var regexS = "[\\?&]"+name+"=([^&#]*)";  
	var regex = new RegExp( regexS );  
	var results = regex.exec( window.location.href ); 
	 if( results == null )    return "";  
	else
	_error = "";
	//alert(errors);
	
	return results[1];
	
	}
	
	function gup_name( err_name ){
	err_name = err_name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");  
	var regexS = "[\\?&]"+err_name+"=([^&#]*)";  
	var regex = new RegExp( regexS );  
	var results = regex.exec( window.location.href ); 
	 if( results == null )    return "";  
	else
	_error_name = "";
	//alert(errors);
	
	return results[1];
	
	}
	
	function gup_name( err_contact ){
	err_contact = err_contact.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");  
	var regexS = "[\\?&]"+err_contact+"=([^&#]*)";  
	var regex = new RegExp( regexS );  
	var results = regex.exec( window.location.href ); 
	 if( results == null )    return "";  
	else
	_error_contact = "";
	//alert(errors);
	
	return results[1];
	
	}
	
	function gup_email( err_email ){
	err_email = err_email.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");  
	var regexS = "[\\?&]"+err_email+"=([^&#]*)";  
	var regex = new RegExp( regexS );  
	var results = regex.exec( window.location.href ); 
	 if( results == null )    return "";  
	else
	_error_email = "";
	//alert(errors);
	
	return results[1];
	
	}
	
    var errors = gup( 'errorc' );
	_error = errors;
	
	 var error_name = gup( 'errorname' );
	_error_name = error_name;
	
	var error_contact = gup( 'errorcontact' );
	_error_contact = error_contact;
	
	var error_email = gup( 'erroremail' );
	_error_email = error_email;
	
	//alert(errors);
	
    </script>
    <!--end jeff-->

<!--start jeff-->
<div style="background-color:#efefef;">
<div style="margin:20px; margin-top:0px;">
<div id="lightbox_title" style="display:inline-block;">Request Quote</div><div style="color:#fd4a4a; font-size:9px; display:inline-block; width:80px; float:right; line-height:30px;">* Required Field</div>
            <div align="center">
            <form method="post" name="form3" style="text-align:left;" action="MailForm2.php">
              <table width="95%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="39%"  valign="top">Name<span style=" color:#fd4a4a">*</span></td>
                      <td  valign="top">:</td>
                      <td  valign="top"><input name="Name" id="Name" type="text" class="form" style="width:250px" value="<?php echo $_SESSION['names']; ?>"/>
                      <br /><label id="lblname" style="color:#fd4a4a; font-size:11px;"></label>
                      </td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td height="10">&nbsp;</td>
                </tr>
                
                <tr>
                  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="39%" valign="top">Company Name</td>
                      <td  valign="top">:</td>
                      <td  valign="top"><input name="CompanyName"  id="CompanyName" type="text" class="form" style="width:250px" value="<?php echo $_SESSION['compnames']; ?>"></td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td height="10">&nbsp;</td>
                </tr>
                <tr>
                  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="39%" valign="top">Contact No<span style=" color:#fd4a4a">*</span></td>
                      <td  valign="top">:</td>
                      <td  valign="top"><input name="ContactNo" id="ContactNo" type="text" class="form" style="width:250px" value="<?php echo $_SESSION['contacts']; ?>">
                      <br /><label id="lblcontact" style="color:#fd4a4a; font-size:11px;"></label>
                      </td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td height="10">&nbsp;</td>
                </tr>               
                <tr>
                  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="39%" valign="top">E-mail Address<span style=" color:#fd4a4a">*</span></td>
                      <td  valign="top">:</td>
                      <td  valign="top"><input name="Email" id="Email" type="text" class="form" style="width:250px" onChange="valEmailIndex2();" value="<?php echo $_SESSION['emails']; ?>">
                      <br /><label id="lblemail" style="color:#fd4a4a; font-size:11px;"></label>
                      </td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td height="10">&nbsp;</td>
                </tr>
                <tr>
                  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="39%" valign="top">Would like to know more on</td>
                      <td  valign="top">:</td>
                      <td  valign="top"><select name="topic" id="topic" class="form" style="width:253px">
                        <option value="">Please Select</option>
                        <?php if($_SESSION['topics'] == "Android - Google Mobile Application Development"){?>
                        <option value="Android - Google Mobile Application Development" selected="selected">Android - Google Mobile Application Development</option>
                        <?php }
						      else
						      {
						?>
                        <option value="Android - Google Mobile Application Development">Android - Google Mobile Application Development</option>
                        <?php } ?>
                        
                        <?php if($_SESSION['topics'] == "BlackBerry - RIM Mobile Application Development"){?>
                         <option value="BlackBerry - RIM Mobile Application Development" selected="selected">BlackBerry - RIM Mobile Application Development</option>
                        <?php }
						      else
						      {
						?>
                         <option value="BlackBerry - RIM Mobile Application Development">BlackBerry - RIM Mobile Application Development</option>
                        <?php } ?>
                        
                        <?php if($_SESSION['topics'] == "iPhone - Apple Mobile Application Development"){?>
                          <option value="iPhone - Apple Mobile Application Development" selected="selected">iPhone - Apple Mobile Application Development</option>
                        <?php }
						      else
						      {
						?>
                          <option value="iPhone - Apple Mobile Application Development">iPhone - Apple Mobile Application Development</option>
                        <?php } ?>
                        
                        <?php if($_SESSION['topics'] == "Windows Mobile Application Development"){?>
                          <option value="Windows Mobile Application Development" selected="selected">Windows Mobile Application Development</option>
                        <?php }
						      else
						      {
						?>
                          <option value="Windows Mobile Application Development">Windows Mobile Application Development</option>
                        <?php } ?>
                        
                        <?php if($_SESSION['topics'] == "Java ME Application Development"){?>
                          <option value="Java ME Application Development" selected="selected">Java ME Application Development</option>
                        <?php }
						      else
						      {
						?>
                          <option value="Java ME Application Development">Java ME Application Development</option>
                        <?php } ?>
                        
                        <?php if($_SESSION['topics'] == "Palm WebOs Mobile Application Development"){?>
                          <option value="Palm WebOs Mobile Application Development" selected="selected">Palm WebOs Mobile Application Development</option>
                        <?php }
						      else
						      {
						?>
                          <option value="Palm WebOs Mobile Application Development">Palm WebOs Mobile Application Development</option>
                        <?php } ?>
                        
                        <?php if($_SESSION['topics'] == "Mobile Game Development"){?>
                          <option value="Mobile Game Development" selected="selected">Mobile Game Development</option>
                        <?php }
						      else
						      {
						?>
                          <option value="Mobile Game Development">Mobile Game Development</option>
                        <?php } ?>
                        
                        <?php if($_SESSION['topics'] == "Others"){?>
                          <option value="Others" selected="selected">Others</option>
                        <?php }
						      else
						      {
						?>
                          <option value="Others">Others</option>
                        <?php } ?>
                      </select>                      </td>
                    </tr>
                  </table></td>
                </tr>

                
                <tr>

                  <td height="10">&nbsp;</td>
                </tr>
                <tr>
                  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="39%" valign="top">Kindly provide a brief of company products</td>
                      <td  valign="top">:</td>
                      <td  valign="top"><textarea name="ProdDesc" id="ProdDesc"  cols="500" rows="3" class="form" style="width:250px"><?php echo $_SESSION['prodescps']; ?></textarea></td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td height="10">&nbsp;</td>
                </tr>
                <tr>
                  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="39%" valign="top">What is your expect time frame for this project?</td>
                      <td  valign="top">:</td>
                      <td  valign="top"><select name="Timeline" id="Timeline" class="form" style="width:253px">
                        <option selected="selected">Please Select</option>
                        <?php if($_SESSION['timelines'] == "1 Week"){?>
                           <option value="1 Week" selected="selected">1 Week</option>
                        <?php }
						      else
						      {
						?>
                           <option value="1 Week">1 Week</option>
                        <?php } ?>
                        
                        <?php if($_SESSION['timelines'] == "2 Week"){?>
                           <option value="2 Week" selected="selected">2 Week</option>
                        <?php }
						      else
						      {
						?>
                           <option value="2 Week">2 Week</option>
                        <?php } ?>
                        
                        <?php if($_SESSION['timelines'] == "3 Week"){?>
                           <option value="3 Week" selected="selected">3 Week</option>
                        <?php }
						      else
						      {
						?>
                           <option value="3 Week">3 Week</option>
                        <?php } ?>
                        
                        <?php if($_SESSION['timelines'] == "1 Month"){?>
                           <option value="1 Month" selected="selected">1 Month</option>
                        <?php }
						      else
						      {
						?>
                           <option value="1 Month">1 Month</option>
                        <?php } ?>
                        
                        <?php if($_SESSION['timelines'] == "2 Month"){?>
                           <option value="2 Month" selected="selected">2 Month</option>
                        <?php }
						      else
						      {
						?>
                           <option value="2 Month">2 Month</option>
                        <?php } ?>
                        
                         <?php if($_SESSION['timelines'] == "3 Months and above"){?>
                           <option value="3 Months and above" selected="selected">3 Months and above</option>
                        <?php }
						      else
						      {
						?>
                           <option value="3 Months and above">3 Months and above</option>
                        <?php } ?>
                                                                  </select></td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td height="10">&nbsp;</td>
                </tr>
                <tr>
                  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="39%" valign="top">What is the main purpose of your Application?</td>
                      <td  valign="top">:</td>
                      <td ><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td valign="top"><select name="Purpose" id="Purpose" class="form" style="width:253px">
                            <option value="">Please Select</option>
                            <?php if($_SESSION['purposes'] == "Attract more customer"){?>
                            <option value="Attract more customer" selected="selected">Attract more customer</option>
                            <?php }
                            else
                            {
                            ?>
                            <option value="Attract more customer">Attract more customer</option>
                            <?php } ?>
                            
                            <?php if($_SESSION['purposes'] == "Selling goods online"){?>
                            <option value="Selling goods online" selected="selected">Selling goods online</option>
                            <?php }
                            else
                            {
                            ?>
                            <option value="Selling goods online">Selling goods online</option>
                            <?php } ?>
                            
                            <?php if($_SESSION['purposes'] == "Extend marketing reach"){?>
                            <option value="Extend marketing reach" selected="selected">Extend marketing reach</option>
                            <?php }
                            else
                            {
                            ?>
                            <option value="Extend marketing reach">Extend marketing reach</option>
                            <?php } ?>
                            
                            <?php if($_SESSION['purposes'] == "Informative"){?>
                            <option value="Informative" selected="selected">Informative</option>
                            <?php }
                            else
                            {
                            ?>
                            <option value="Informative">Informative</option>
                            <?php } ?>
                            
                            <?php if($_SESSION['purposes'] == "A brief of my company"){?>
                            <option value="A brief of my company" selected="selected">A brief of my company</option>
                            <?php }
                            else
                            {
                            ?>
                            <option value="A brief of my company">A brief of my company</option>
                            <?php } ?>
                        
                            <?php if($_SESSION['purposes'] == "Other, please state at below"){?>
                            <option value="Other, please state at below" selected="selected">Other, please state at below</option>
                            <?php }
                            else
                            {
                            ?>
                            <option value="Other, please state at below">Other, please state at below</option>
                            <?php } ?>
                                                                              </select></td>
                        </tr>
                        
                        <tr>
                          <td valign="top"><textarea name="OtherPurpose" id="OtherPurpose" cols="500" rows="3" class="form" style="width:250px"><?php echo $_SESSION['otherpurposes']; ?></textarea></td>
                        </tr>
                      </table></td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td height="10">&nbsp;</td>
                </tr>
                <tr>
                  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="39%" valign="top">How much is your budget?</td>
                      <td  valign="top">:</td>
                      <td valign="middle"><select name="Purpose2" id="Purpose2" class="form" style="width:253px;">
                            <option value="">Please Select</option>
                      <?php if($_SESSION['purpose2s'] == "Below RM10,000"){?>
                            <option value="Below RM10,000" selected="selected">Below RM10,000</option>
                            <?php }
                            else
                            {
                            ?>
                            <option value="Below RM10,000">Below RM10,000</option>
                            <?php } ?>
                            
                            <?php if($_SESSION['purpose2s'] == "10,000–15,000"){?>
                            <option value="10,000-15,000" selected="selected">10,000-15,000</option>
                            <?php }
                            else
                            {
                            ?>
                            <option value="10,000-15,000">10,000-15,000</option>
                            <?php } ?>
                            
                            <?php if($_SESSION['purpose2s'] == "15,001-25,000"){?>
                            <option value="15,001-25,000" selected="selected">15,001-25,000</option>
                            <?php }
                            else
                            {
                            ?>
                            <option value="15,001-25,000">15,001-25,000</option>
                            <?php } ?>
                            
                            <?php if($_SESSION['purpose2s'] == "25,001 and above"){?>
                            <option value="25,001 and above" selected="selected">25,001 and above</option>
                            <?php }
                            else
                            {
                            ?>
                            <option value="25,001 and above">25,001 and above</option>
                            <?php } ?>
                            </select></td>
                      <!--<td width="59%" valign="top"><input name="Budget" type="text" class="form" style="width:250px" /></td>-->
                    </tr>
                  </table></td>
                </tr>
                <!--<tr>
                <td height="25">&nbsp;</td>
                </tr>-->
                
                 
                <!--<tr>
                <td style="padding-top:10px; padding-bottom:10px;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                <td width="39%" valign="top">Enter the code shown</td>
                <td  valign="top">:</td>
                <td valign="middle">
                <img src="captcha_code_file.php?rand=<?php echo rand(); ?>" id='captchaimg' ><br>
                <label for=''></label>
                <input id="6_letters_code" name="6_letters_code" type="text"><br>
                <label for=''></label>
                <small>Can't read the image? click <a href='javascript: refreshCaptcha();'>here</a> to refresh</small>
                </td>
                </tr>
                </table>
                </td>
                </tr>-->
                
                    <!-- start jeff captcha code generated -->
                    <tr><td style="padding-top:10px; padding-bottom:10px;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
                    <td width="39%" valign="top"></td>
                    <td valign="top"></td>
                    <td height="25" valign="middle" style="padding-left:10px;">
                    
                    <label for='message'>Please solve this</label><br>
                    <input type="text" name="captchaImage" size="5" value="<?= $randomNum ?> + <?= $randomNum2 ?>" disabled="disabled" style="text-align:center; font-weight:bold;"/> =
                    <input type="text" name="math" id="math" size="6" maxlength="6" />
                    <label id="errtxt" style="color:#fd4a4a"></label><br />
                    
                    </td>
                    </tr></table>
                    </td></tr>
                    <!-- end jeff-->
               
                
                <!--MM_validateForm('Name','','R','ContactNo','','R','Email','','RisEmail');return document.MM_returnValue-->
                <tr>
                <td >
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                <td width="39%" ></td>
                <td ></td>
                  <td align="left" style="padding-left:0px;"><input name="Submit" id="submit" type="submit" value="Click here to request for a quotation" /></td>
                  </tr>
                  </table>
                </td>
                </tr>
              </table>
              </form>
            </div>
        </div>
        </div>
        <!---Quotation Form End--->
</div>

<script language='JavaScript' type='text/javascript'>
function refreshCaptcha()
{
	var img = document.images['captchaimg'];
	img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
}
</script>
</body>
</html>
